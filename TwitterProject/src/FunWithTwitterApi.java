import java.util.ArrayList;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

import twitter4j.Location;
import twitter4j.ResponseList;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class FunWithTwitterApi {

	/**
	 * @param args
	 */
	
	/*
	 * Be super careful running the following program if you have rate limitations (as the app with the api and token keys I defined was rate limited)
	 * Twitter imposes heavy limitations on getting trends, and this program gets the top 10 trends associated with a specific trending area in a user chosen country
	 * I have a twitter4j.properties text file located at the root of my project that defines all of the oauth.consumerKey, oauth.consumerSecret, oauth.accessToken, and oauth.accessTokenSecret
	 */
	public static void main(String[] args) {
		try {
			/*
			 * class variables
			 */
			
			//the following lines initialize appropriate twitter objects
			Twitter twitter = new TwitterFactory().getInstance();
			ResponseList<Location> locations = twitter.getAvailableTrends();
			
			//the following maps are used for ease (two maps aren't currently used, but in a hopeful future enhancement they will be)
			TreeMap<String, ArrayList<Object[]>> map = new TreeMap<String, ArrayList<Object[]>>();
			TreeMap<String, Integer> trendMap = new TreeMap<String, Integer>();
			TreeMap<String, Integer> trendMapForCountry = new TreeMap<String, Integer>();
			String countryName = "";
			String temp1 = "";
			String temp2 = "";
			int woeid = 0;
			
			//for each location returned, fill the map with the desired parameters, name country name as a key and a list of all region's with their names and woeids
			for (Location location : locations) {
				fillMap(map, location.getWoeid(), location.getName(),
						location.getCountryName());
			}
			
			/*
			 * user input section that asks the user what country he or she wishes to search
			 * the user is then given a list of regions that are trending in that country, and he is prompted
			 * to input a woeid to receive the top 10 trends in that specific region
			 * 
			 * TODO: make input more secure, apply development standards to limit rate limiting (probably by transferring to Streaming API
			 * VERY IMPORTANT TODO: make languages work the right way (right now if you search for japan's or saudi arabia's tweets for example, you get nonsense characters)
			 */
			while (true) {
				System.out
						.println("Please input the name of a country you wish to search (if you are getting this prompt again, it means the previous input wasn't a valid country name ");
				System.out.println("");
				System.out.println("Please only use lower case letters, for example: united states");
				Scanner scan = new Scanner(System.in);
				countryName = scan.nextLine();
				
				
				//if the map has the user specified country, we print out the regions and prompt the user to enter a regional woeid to get the trends
				if (map.get(countryName) != null) {
					printList(map, countryName);
					System.out
							.println("Please input one of the regions' woeids: ");
					// Scanner scan2 = new Scanner(System.in);
					woeid = scan.nextInt();
					Trend[] placeTrends = twitter.getPlaceTrends(woeid)
							.getTrends();
					System.out
							.println("Here are the ten trending tweets for your specified designated area:");
					for (int i = 0; i < placeTrends.length; i++) {
						System.out.println(i + 1 + ": " + placeTrends[i]);
					}

					scan.close();
					break;
				}
			}
			
			
			/*
			 * THE BELOW CODE IS A TEST RUN; DO NOT RUN IT!
			 * 
			 * YOU WILL GET RATE LIMITED!
			 * 
			 * I JUST WANTED TO PLAY AROUND WITH BEING CUTE WITH TRENDS
			 * 
			 * TODO: alter logic to use Streaming API to stop being so severely
			 * limited and get geographically distributed trends for all
			 * countries
			 */

			/***********************************************************************************************************************************/
			
//			for (Entry<String, ArrayList<Object[]>> entry : map.entrySet()) {
//				String key = entry.getKey();
//				ArrayList<Object[]> list = entry.getValue();
//
//				for (int i = 0; i < list.size(); i++) {
//					Trend[] trendArray = twitter.getPlaceTrends(
//							(Integer) list.get(i)[0]).getTrends();
//					for (int j = 0; j < trendArray.length; j++) {
//						if (trendMap.get(trendArray[j].getName()) == null) {
//							trendMap.put(trendArray[j].getName(), 1);
//						} else {
//							trendMap.put(trendArray[j].getName(),
//									trendMap.get(trendArray[j].getName()) + 1);
//						}
//					}
//				}
//				Integer maxEntry = 0;
//				String maxEntryName = "null";
//				String mapValue = "";
//				for (Entry<String, Integer> entry2 : trendMap.entrySet()) {
//					String string = entry2.getKey();
//					Integer integer = entry2.getValue();
//
//					if (maxEntry == null
//							|| integer.compareTo(maxEntry.intValue()) > 0) {
//						maxEntry = integer;
//						maxEntryName = string;
//					}
//				}
//				mapValue = key + "'s most popular trend across regions is: "
//						+ maxEntryName;
//				System.out.println(mapValue + " with a count of- " + maxEntry);
//				// trendMapForCountry.put(mapValue, maxEntry);
//			}
			 

			/***********************************************************************************************************************************/
			System.out.println("done.");
			System.exit(0);
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to get trends: " + te.getMessage());
			System.exit(-1);
		}

	}

	//custom print method to show exactly the country name, the region name, and the woeid associated with the region in that country
	private static void printList(TreeMap<String, ArrayList<Object[]>> map,
			String countryName) {
		System.out.println(countryName + "'s trending regions are: ");
		System.out.println("");
		
		//the object array is custom designed to hold the name in the second slot and the woeid in the first slot
		for (int i = 0; i < map.get(countryName).size(); i++) {
			System.out.println(i + 1 + "- Place name: "
					+ map.get(countryName).get(i)[1] + " and woeid: "
					+ map.get(countryName).get(i)[0]);
		}

	}

	//fills a custom map with the country name as a key and a list of regions and woeids associated with that country
	
	private static void fillMap(TreeMap<String, ArrayList<Object[]>> map,
			int id, String placeName, String countryName) {
		String name = countryName.toLowerCase();
		Object[] parameters = { id, placeName };
		if (map.get(name) == null) {
			ArrayList<Object[]> list = new ArrayList<Object[]>();

			list.add(parameters);
			map.put(name, list);
		} else {
			map.get(name).add(parameters);
		}
	}

}
