import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AppleInformation {

	/*
	 * Purpose: to list out phone numbers on a specific page
	 * Method: 1.) make a url connection to the desired website
	 * 		   2.) read in, line by line, the source of that page into a Java object (BufferedReader)
	 *         3.) Go through the Java object line by line and search for a particular regular expression
	 *         4.) The regular expressions are those for (Usa and Canada) and (Mexico) phone number formats
	 *         5.) Since Mexican formats encompass the other, search for Mexican first (to avoid double-dipping)
	 *         6.) If a format is found, print it out
	 */
	public static void main(String[] args) throws IOException {
		
		//make a url connection with the desired web page, in this case apple
		String url = "http://www.apple.com/contact/";
		
		//I also tested with a generic phone number listing page with many phone numbers; comment out apple line and uncomment following line if you want to test that
		//String url = "http://www.archives.gov/about/organization/telephone-list.html";
		URL urlObject = new URL(url);
		URLConnection urlConn = urlObject.openConnection();
		
		//read the full html of the page into an input stream
		BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
		String inputLine;
		
		System.out.println("The phone numbers listed on " + url + " are: ");
		System.out.println();
		
		while ((inputLine = in.readLine()) != null) {
			String stringToSearch = inputLine;
			 
			//regular expression for various USA and Canadian phone formats
		    Pattern naNumber = Pattern.compile("(\\(?\\b[0-9]{3}\\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}\\b)");
		    
		    //regular expression for various Mexican phone formats
			Pattern mexicoNumber = Pattern.compile("(\\b[0-9]{3}[-. ]?[0-9]{3}[-. ]?[0-9]{3}[-. ]?[0-9]{4}\\b)");
			
			//searches for the pattern in the given string
		    Matcher naM = naNumber.matcher(stringToSearch);
		    Matcher mexicoM = mexicoNumber.matcher(stringToSearch);
		    
		    //since Mexico format essentially encompasses a US or Canadian format, search for Mexican format first
		    if (mexicoM.find()){	       
		    	System.out.println("Mexican #: " + mexicoM.group(1));
		    }
		    else if(naM.find()) {
		    	String na = naM.group(1);
		    	//remove leading ( if dangling
		    	if(naM.group(1).charAt(0)=='(' && naM.group(1).charAt(4)!=')') na = na.substring(1, na.length());
		    	System.out.println("USA or Canada #: " + na);
		    }
		    
	//TODO add functionality to list multiple phone numbers given on the same line in the input buffer stream
	//TODO potentially add functionality to list only unique phone numbers, or list how many instances a phone number appears
    //TODO formats for all other regions (right now only does North America)
		
		}
		
		in.close();		
	}
}
