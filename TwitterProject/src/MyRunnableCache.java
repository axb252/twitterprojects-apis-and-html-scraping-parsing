import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.TreeMap;


public class MyRunnableCache implements Runnable {
    
	private final String url;
	int counter = 0;
    MyRunnableCache(String url, int counter) {
        this.url = url;
        this.counter = counter;
    }

    @Override
    public void run() {

    	// create a new url connection with the extracted website's url
		URL obj = null;
		try {
			obj = new URL(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		URLConnection conn;
		try {
			conn = obj.openConnection();
			//the following header fields are the standard attributes set for sites
			//Cache-Control is generally an universal catch all but sometimes specific attributes are explicitly set
			String value = conn.getHeaderField("cache-control");
			String value2 = conn.getHeaderField("pragma");
			String value3 = conn.getHeaderField("vary");
			String value4 = conn.getHeaderField("expires");
			String value5= conn.getHeaderField("etag");
			
			String temp = counter + ".) " + url
					+ "'s settings are: Cache-Control - " + value + "| pragma- " + value2
					+ "| vary- " + value3 + "| expires- " + value4 + "| etag- " + value5;
			
			File file =new File("sites.txt");
			
			FileWriter fileWritter = new FileWriter(file.getName(),true);
	        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
	        bufferWritter.write(temp);
	        bufferWritter.newLine();
	        bufferWritter.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
    }
{

}
}
