
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/*
 * Project Purpose: to list out the cache control settings of the top 100 websites on alexa.com
 */
public class CacheControlDeterminator {
	private static final int MYTHREADS = 5;
	
	public static void main(String[] args) throws Exception {
		
		int counter = 0;
		int desiredValue = 100; // how many sites you want to grab, change to any integer value in between 1 and 500 inclusive
		if(desiredValue > 500) {
			desiredValue = 500; //alexa only has top 500 so keeps value legal
		}
		else if(desiredValue < 1) {
			desiredValue = 1; //minimum value is 1
		}
		int mod = (desiredValue + 24) / 25; // alexa sorts its top 500 by 25 per page (+24 because ints truncate in java)

		System.out.println("The cache control settings for the top " + desiredValue + " websites according to alexa.com are:");
		System.out.println("");
		ArrayList<String> siteList = new ArrayList<String>();
		// loop opens a url connection to each page required to get the desired value of websites
		for (int i = 0; i < mod; i++) {

			// ex. global;0 represents 1-25, global;1 represents 26 - 50 etc.
			//so for 100 websites, we would need global;0, global;1, global;2, and global;3 called explicitly
			String url = "http://www.alexa.com/topsites/global;" + i;
			
			//open a url connection to the proper alexa page
			URL alexa = new URL(url);
			URLConnection alexaconn = alexa.openConnection();
			
			//read the full html of the alexa page into an input stream
			BufferedReader in = new BufferedReader(new InputStreamReader(alexaconn.getInputStream()));
			String inputLine;

			//preprocessing site list into an ArrayList
			while ((inputLine = in.readLine()) != null) {
				// this is the alexa specific unique identifier for the line with the website's url
				if (inputLine.startsWith("<a href=\"/siteinfo/")) {
					// following 3 lines parse out the desired website url
					int first = inputLine.indexOf('>');
					int last = inputLine.indexOf('<', first);
					
					//add the stringified url to our list of sites 
					String test = "http://www." + inputLine.substring(first + 1, last);
					siteList.add(test);
				}
				
			}
			in.close();
		}
		
		//check the root of the project for this file
		File file =new File("sites.txt");
		 
		//if file doesn't exist, create it; if it does, delete it so a fresh one can be made
		if(file.exists()) file.delete();
		file.createNewFile();
		
		//creates an executor service to split work into numerous threads to significantly improve performance 
		for(int i = 0;i < siteList.size();i=i+5){
			ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
			for(int j = i; j <i+5;j++){
				String url = siteList.get(j);
	            Runnable worker = new MyRunnableCache(url, j+1);
	            executor.execute(worker); 
	            
			}
			 executor.shutdown();
			 executor.awaitTermination(200, TimeUnit.MILLISECONDS);
		}	
		
		System.out.println("Check the root of the project to view sites.txt, which should have the settings listed.");
		
	}
}