
import java.io.*;
import java.net.URL;
import java.net.URLConnection;


public class CacheControlDeterminator2 {

	public static void main(String[] args) throws Exception {

		int counter = 0;
		int desiredValue = 100; // how many sites you want to grab, change to any integer value in between 1 and 500 inclusive
		if(desiredValue > 500) {
			desiredValue = 500; //alexa only has top 500 so keeps value legal
		}
		else if(desiredValue < 1) {
			desiredValue = 1; //minimum value is 1
		}
		int mod = (desiredValue + 24) / 25; // alexa sorts its top 500 by 25 per page (+24 because ints truncate in java)

		System.out.println("The cache control settings for the top " + desiredValue + " websites according to alexa.com are:");
		System.out.println("");
		
		// loop opens a url connection to each page required to get the desired value of websites
		for (int i = 0; i < mod; i++) {

			// ex. global;0 represents 1-25, global;1 represents 26 - 50 etc.
			//so for 100 websites, we would need global;0, global;1, global;2, and global;3 called explicitly
			String url = "http://www.alexa.com/topsites/global;" + i;
			
			//open a url connection to the proper alexa page
			URL alexa = new URL(url);
			URLConnection alexaconn = alexa.openConnection();
			
			//read the full html of the alexa page into an input stream
			BufferedReader in = new BufferedReader(new InputStreamReader(alexaconn.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				// this is the alexa specific unique identifier for the line with the website's url
				if (inputLine.startsWith("<a href=\"/siteinfo/")) {

					// following 3 lines parse out the desired website url
					int first = inputLine.indexOf('>');
					int last = inputLine.indexOf('<', first);
					String test = "http://www." + inputLine.substring(first + 1, last);

					// create a new url connection with the extracted website's url
					URL obj = new URL(test);
					URLConnection conn = obj.openConnection();
					
					//the following header fields are the standard attributes set for sites
					//Cache-Control is generally an universal catch all but sometimes specific attributes are explicitly set
					String value = conn.getHeaderField("cache-control");
					String value2 = conn.getHeaderField("pragma");
					String value3 = conn.getHeaderField("vary");
					String value4 = conn.getHeaderField("expires");
					String value5= conn.getHeaderField("etag");
					
					//following two lines ensure we only print out desired number of sites
					counter++;
					if (counter > desiredValue) break;
					
					//print out extracted information
					System.out.println(counter + ".) " + test
							+ "'s settings are: Cache-Control - " + value + "| pragma- " + value2
							+ "| vary- " + value3 + "| expires- " + value4 + "| etag- " + value5);
				}
			}
			in.close();
		}
	}
}